﻿namespace Digital_Secretary
{
    partial class frmNoteBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNoteBook));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pnlSaveNote = new System.Windows.Forms.Panel();
            this.lblSaveNote = new System.Windows.Forms.Label();
            this.pbSaveNote = new System.Windows.Forms.PictureBox();
            this.pnlDiscardNote = new System.Windows.Forms.Panel();
            this.lblDiscardNote = new System.Windows.Forms.Label();
            this.pbDiscardNote = new System.Windows.Forms.PictureBox();
            this.txtName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.rtxtNote = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.pnlAllNotes = new System.Windows.Forms.Panel();
            this.lstviewAllNotes = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FirstColumn = new System.Windows.Forms.Timer(this.components);
            this.pnlRightOptions = new System.Windows.Forms.Panel();
            this.pnlRightMain = new System.Windows.Forms.Panel();
            this.pbToggleSpeech = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pbExit = new System.Windows.Forms.PictureBox();
            this.pbHome = new System.Windows.Forms.PictureBox();
            this.pbLogout = new System.Windows.Forms.PictureBox();
            this.pbExitApplication = new System.Windows.Forms.PictureBox();
            this.pbMinimize = new System.Windows.Forms.PictureBox();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pnlSaveNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSaveNote)).BeginInit();
            this.pnlDiscardNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiscardNote)).BeginInit();
            this.pnlAllNotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRightOptions.SuspendLayout();
            this.pnlRightMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbToggleSpeech)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExitApplication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.Transparent;
            this.pnlMain.Controls.Add(this.pictureBox5);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.pictureBox4);
            this.pnlMain.Controls.Add(this.pnlSaveNote);
            this.pnlMain.Controls.Add(this.pnlDiscardNote);
            this.pnlMain.Controls.Add(this.txtName);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.rtxtNote);
            this.pnlMain.Controls.Add(this.pnlAllNotes);
            this.pnlMain.Controls.Add(this.pictureBox1);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.ForeColor = System.Drawing.Color.Black;
            this.pnlMain.Location = new System.Drawing.Point(1, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1025, 702);
            this.pnlMain.TabIndex = 16;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(629, 40);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(37, 29);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 31;
            this.pictureBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(664, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 28);
            this.label3.TabIndex = 30;
            this.label3.Text = "YOUR NOTEBOOK";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(986, 103);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(43, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 29;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseEnter += new System.EventHandler(this.pictureBox4_MouseEnter);
            this.pictureBox4.MouseLeave += new System.EventHandler(this.pictureBox4_MouseLeave);
            // 
            // pnlSaveNote
            // 
            this.pnlSaveNote.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlSaveNote.Controls.Add(this.lblSaveNote);
            this.pnlSaveNote.Controls.Add(this.pbSaveNote);
            this.pnlSaveNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlSaveNote.Location = new System.Drawing.Point(760, 555);
            this.pnlSaveNote.Name = "pnlSaveNote";
            this.pnlSaveNote.Size = new System.Drawing.Size(239, 51);
            this.pnlSaveNote.TabIndex = 27;
            // 
            // lblSaveNote
            // 
            this.lblSaveNote.AutoSize = true;
            this.lblSaveNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSaveNote.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaveNote.Location = new System.Drawing.Point(79, 21);
            this.lblSaveNote.Name = "lblSaveNote";
            this.lblSaveNote.Size = new System.Drawing.Size(132, 23);
            this.lblSaveNote.TabIndex = 1;
            this.lblSaveNote.Text = "SAVE CHANGES";
            // 
            // pbSaveNote
            // 
            this.pbSaveNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbSaveNote.Image = ((System.Drawing.Image)(resources.GetObject("pbSaveNote.Image")));
            this.pbSaveNote.Location = new System.Drawing.Point(0, 3);
            this.pbSaveNote.Name = "pbSaveNote";
            this.pbSaveNote.Size = new System.Drawing.Size(73, 48);
            this.pbSaveNote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSaveNote.TabIndex = 0;
            this.pbSaveNote.TabStop = false;
            // 
            // pnlDiscardNote
            // 
            this.pnlDiscardNote.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlDiscardNote.Controls.Add(this.lblDiscardNote);
            this.pnlDiscardNote.Controls.Add(this.pbDiscardNote);
            this.pnlDiscardNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlDiscardNote.Location = new System.Drawing.Point(476, 555);
            this.pnlDiscardNote.Name = "pnlDiscardNote";
            this.pnlDiscardNote.Size = new System.Drawing.Size(239, 51);
            this.pnlDiscardNote.TabIndex = 28;
            // 
            // lblDiscardNote
            // 
            this.lblDiscardNote.AutoSize = true;
            this.lblDiscardNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDiscardNote.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscardNote.Location = new System.Drawing.Point(74, 21);
            this.lblDiscardNote.Name = "lblDiscardNote";
            this.lblDiscardNote.Size = new System.Drawing.Size(162, 23);
            this.lblDiscardNote.TabIndex = 1;
            this.lblDiscardNote.Text = "DISCARD CHANGES";
            // 
            // pbDiscardNote
            // 
            this.pbDiscardNote.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbDiscardNote.Image = ((System.Drawing.Image)(resources.GetObject("pbDiscardNote.Image")));
            this.pbDiscardNote.Location = new System.Drawing.Point(0, 0);
            this.pbDiscardNote.Name = "pbDiscardNote";
            this.pbDiscardNote.Size = new System.Drawing.Size(73, 51);
            this.pbDiscardNote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDiscardNote.TabIndex = 0;
            this.pbDiscardNote.TabStop = false;
            // 
            // txtName
            // 
            // 
            // 
            // 
            this.txtName.Border.Class = "TextBoxBorder";
            this.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtName.Location = new System.Drawing.Point(500, 100);
            this.txtName.Name = "txtName";
            this.txtName.PreventEnterBeep = true;
            this.txtName.Size = new System.Drawing.Size(480, 35);
            this.txtName.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(449, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 30);
            this.label2.TabIndex = 24;
            this.label2.Text = "FILE";
            // 
            // rtxtNote
            // 
            // 
            // 
            // 
            this.rtxtNote.BackgroundStyle.Class = "RichTextBoxBorder";
            this.rtxtNote.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rtxtNote.Location = new System.Drawing.Point(454, 149);
            this.rtxtNote.Name = "rtxtNote";
            this.rtxtNote.Rtf = "{\\rtf1\\ansi\\deff0{\\fonttbl{\\f0\\fnil\\fcharset0 Segoe UI;}}\r\n\\viewkind4\\uc1\\pard\\la" +
    "ng1033\\f0\\fs32 richTextBoxEx1\\par\r\n}\r\n";
            this.rtxtNote.Size = new System.Drawing.Size(571, 400);
            this.rtxtNote.TabIndex = 23;
            this.rtxtNote.Text = "richTextBoxEx1";
            // 
            // pnlAllNotes
            // 
            this.pnlAllNotes.Controls.Add(this.lstviewAllNotes);
            this.pnlAllNotes.Location = new System.Drawing.Point(37, 102);
            this.pnlAllNotes.Name = "pnlAllNotes";
            this.pnlAllNotes.Size = new System.Drawing.Size(397, 500);
            this.pnlAllNotes.TabIndex = 22;
            // 
            // lstviewAllNotes
            // 
            this.lstviewAllNotes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstviewAllNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstviewAllNotes.Location = new System.Drawing.Point(0, 0);
            this.lstviewAllNotes.Name = "lstviewAllNotes";
            this.lstviewAllNotes.Size = new System.Drawing.Size(397, 500);
            this.lstviewAllNotes.TabIndex = 1;
            this.lstviewAllNotes.UseCompatibleStateImageBehavior = false;
            this.lstviewAllNotes.View = System.Windows.Forms.View.Details;
            this.lstviewAllNotes.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstviewAllNotes_ItemChecked);
            this.lstviewAllNotes.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstviewAllNotes_ItemSelectionChanged);
            this.lstviewAllNotes.Click += new System.EventHandler(this.lstviewAllNotes_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 290;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Created";
            this.columnHeader2.Width = 110;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(37, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(128, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(380, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "DIGITAL SECRETARY";
            // 
            // FirstColumn
            // 
            this.FirstColumn.Interval = 2000;
            // 
            // pnlRightOptions
            // 
            this.pnlRightOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRightOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pnlRightOptions.Controls.Add(this.pnlRightMain);
            this.pnlRightOptions.Location = new System.Drawing.Point(903, 12);
            this.pnlRightOptions.Name = "pnlRightOptions";
            this.pnlRightOptions.Size = new System.Drawing.Size(72, 721);
            this.pnlRightOptions.TabIndex = 19;
            // 
            // pnlRightMain
            // 
            this.pnlRightMain.Controls.Add(this.pbToggleSpeech);
            this.pnlRightMain.Controls.Add(this.pictureBox2);
            this.pnlRightMain.Controls.Add(this.pbExit);
            this.pnlRightMain.Controls.Add(this.pbHome);
            this.pnlRightMain.Controls.Add(this.pbLogout);
            this.pnlRightMain.Location = new System.Drawing.Point(3, 52);
            this.pnlRightMain.Name = "pnlRightMain";
            this.pnlRightMain.Size = new System.Drawing.Size(70, 591);
            this.pnlRightMain.TabIndex = 1;
            this.pnlRightMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlRightMain_Paint);
            // 
            // pbToggleSpeech
            // 
            this.pbToggleSpeech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbToggleSpeech.Image = ((System.Drawing.Image)(resources.GetObject("pbToggleSpeech.Image")));
            this.pbToggleSpeech.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbToggleSpeech.Location = new System.Drawing.Point(0, 258);
            this.pbToggleSpeech.Name = "pbToggleSpeech";
            this.pbToggleSpeech.Size = new System.Drawing.Size(70, 76);
            this.pbToggleSpeech.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbToggleSpeech.TabIndex = 5;
            this.pbToggleSpeech.TabStop = false;
            this.pbToggleSpeech.MouseEnter += new System.EventHandler(this.pbToggleSpeech_MouseEnter);
            this.pbToggleSpeech.MouseLeave += new System.EventHandler(this.pbToggleSpeech_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 512);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pbExit
            // 
            this.pbExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbExit.Image = ((System.Drawing.Image)(resources.GetObject("pbExit.Image")));
            this.pbExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbExit.Location = new System.Drawing.Point(-3, 388);
            this.pbExit.Name = "pbExit";
            this.pbExit.Size = new System.Drawing.Size(76, 76);
            this.pbExit.TabIndex = 5;
            this.pbExit.TabStop = false;
            // 
            // pbHome
            // 
            this.pbHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHome.Image = ((System.Drawing.Image)(resources.GetObject("pbHome.Image")));
            this.pbHome.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbHome.Location = new System.Drawing.Point(-3, 131);
            this.pbHome.Name = "pbHome";
            this.pbHome.Size = new System.Drawing.Size(76, 76);
            this.pbHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbHome.TabIndex = 4;
            this.pbHome.TabStop = false;
            this.pbHome.Click += new System.EventHandler(this.pbHome_Click);
            this.pbHome.MouseEnter += new System.EventHandler(this.pbHome_MouseEnter);
            this.pbHome.MouseLeave += new System.EventHandler(this.pbHome_MouseLeave);
            // 
            // pbLogout
            // 
            this.pbLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbLogout.Image = ((System.Drawing.Image)(resources.GetObject("pbLogout.Image")));
            this.pbLogout.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbLogout.Location = new System.Drawing.Point(0, 3);
            this.pbLogout.Name = "pbLogout";
            this.pbLogout.Size = new System.Drawing.Size(69, 76);
            this.pbLogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbLogout.TabIndex = 3;
            this.pbLogout.TabStop = false;
            this.pbLogout.MouseEnter += new System.EventHandler(this.pbLogout_MouseEnter);
            this.pbLogout.MouseLeave += new System.EventHandler(this.pbLogout_MouseLeave);
            // 
            // pbExitApplication
            // 
            this.pbExitApplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbExitApplication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbExitApplication.Image = ((System.Drawing.Image)(resources.GetObject("pbExitApplication.Image")));
            this.pbExitApplication.Location = new System.Drawing.Point(879, -31);
            this.pbExitApplication.Name = "pbExitApplication";
            this.pbExitApplication.Size = new System.Drawing.Size(43, 40);
            this.pbExitApplication.TabIndex = 2;
            this.pbExitApplication.TabStop = false;
            this.pbExitApplication.Click += new System.EventHandler(this.pbExitApplication_Click);
            this.pbExitApplication.MouseEnter += new System.EventHandler(this.pbExitApplication_MouseEnter);
            this.pbExitApplication.MouseLeave += new System.EventHandler(this.pbExitApplication_MouseLeave);
            this.pbExitApplication.MouseHover += new System.EventHandler(this.pbExitApplication_MouseHover);
            // 
            // pbMinimize
            // 
            this.pbMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbMinimize.Image = ((System.Drawing.Image)(resources.GetObject("pbMinimize.Image")));
            this.pbMinimize.Location = new System.Drawing.Point(830, -21);
            this.pbMinimize.Name = "pbMinimize";
            this.pbMinimize.Size = new System.Drawing.Size(43, 40);
            this.pbMinimize.TabIndex = 2;
            this.pbMinimize.TabStop = false;
            this.pbMinimize.Click += new System.EventHandler(this.pbMinimize_Click);
            this.pbMinimize.MouseEnter += new System.EventHandler(this.pbMinimize_MouseEnter);
            this.pbMinimize.MouseLeave += new System.EventHandler(this.pbMinimize_MouseLeave);
            // 
            // frmNoteBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 700);
            this.Controls.Add(this.pbMinimize);
            this.Controls.Add(this.pbExitApplication);
            this.Controls.Add(this.pnlRightOptions);
            this.Controls.Add(this.pnlMain);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "frmNoteBook";
            this.Text = "Digital Secretary";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pnlSaveNote.ResumeLayout(false);
            this.pnlSaveNote.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSaveNote)).EndInit();
            this.pnlDiscardNote.ResumeLayout(false);
            this.pnlDiscardNote.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiscardNote)).EndInit();
            this.pnlAllNotes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRightOptions.ResumeLayout(false);
            this.pnlRightMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbToggleSpeech)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExitApplication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer FirstColumn;
        private System.Windows.Forms.Panel pnlRightOptions;
        private System.Windows.Forms.Panel pnlRightMain;
        private System.Windows.Forms.PictureBox pbExit;
        private System.Windows.Forms.PictureBox pbHome;
        private System.Windows.Forms.PictureBox pbLogout;
        private System.Windows.Forms.PictureBox pbExitApplication;
        private System.Windows.Forms.PictureBox pbMinimize;
        private System.Windows.Forms.PictureBox pbToggleSpeech;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel pnlAllNotes;
        private System.Windows.Forms.ListView lstviewAllNotes;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx rtxtNote;
        private DevComponents.DotNetBar.Controls.TextBoxX txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlSaveNote;
        private System.Windows.Forms.Label lblSaveNote;
        private System.Windows.Forms.PictureBox pbSaveNote;
        private System.Windows.Forms.Panel pnlDiscardNote;
        private System.Windows.Forms.Label lblDiscardNote;
        private System.Windows.Forms.PictureBox pbDiscardNote;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label3;
    }
}

