﻿namespace Digital_Secretary
{
    partial class frmNewReminder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewReminder));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlCreate = new System.Windows.Forms.Panel();
            this.lblCreate = new System.Windows.Forms.Label();
            this.pbCreate = new System.Windows.Forms.PictureBox();
            this.pnlDiscard = new System.Windows.Forms.Panel();
            this.lblDiscard = new System.Windows.Forms.Label();
            this.pbDiscard = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlRightOptions = new System.Windows.Forms.Panel();
            this.pnlRightMain = new System.Windows.Forms.Panel();
            this.pbToggleSpeech = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pbExit = new System.Windows.Forms.PictureBox();
            this.pbHome = new System.Windows.Forms.PictureBox();
            this.pbLogout = new System.Windows.Forms.PictureBox();
            this.pbExitApplication = new System.Windows.Forms.PictureBox();
            this.pbMinimize = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rtxtNote = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.txtTitle = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateTime = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tmTime = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.pnlMain.SuspendLayout();
            this.pnlCreate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCreate)).BeginInit();
            this.pnlDiscard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiscard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRightOptions.SuspendLayout();
            this.pnlRightMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbToggleSpeech)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExitApplication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmTime)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.Transparent;
            this.pnlMain.Controls.Add(this.tmTime);
            this.pnlMain.Controls.Add(this.dtDateTime);
            this.pnlMain.Controls.Add(this.pnlCreate);
            this.pnlMain.Controls.Add(this.pnlDiscard);
            this.pnlMain.Controls.Add(this.rtxtNote);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.txtTitle);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.pictureBox4);
            this.pnlMain.Controls.Add(this.pictureBox1);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.ForeColor = System.Drawing.Color.Black;
            this.pnlMain.Location = new System.Drawing.Point(1, -3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1023, 705);
            this.pnlMain.TabIndex = 16;
            // 
            // pnlCreate
            // 
            this.pnlCreate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlCreate.Controls.Add(this.lblCreate);
            this.pnlCreate.Controls.Add(this.pbCreate);
            this.pnlCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlCreate.Location = new System.Drawing.Point(552, 562);
            this.pnlCreate.Name = "pnlCreate";
            this.pnlCreate.Size = new System.Drawing.Size(239, 58);
            this.pnlCreate.TabIndex = 26;
            // 
            // lblCreate
            // 
            this.lblCreate.AutoSize = true;
            this.lblCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblCreate.Location = new System.Drawing.Point(79, 21);
            this.lblCreate.Name = "lblCreate";
            this.lblCreate.Size = new System.Drawing.Size(86, 30);
            this.lblCreate.TabIndex = 1;
            this.lblCreate.Text = "CREATE";
            // 
            // pbCreate
            // 
            this.pbCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCreate.Image = ((System.Drawing.Image)(resources.GetObject("pbCreate.Image")));
            this.pbCreate.Location = new System.Drawing.Point(-9, 0);
            this.pbCreate.Name = "pbCreate";
            this.pbCreate.Size = new System.Drawing.Size(82, 58);
            this.pbCreate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCreate.TabIndex = 0;
            this.pbCreate.TabStop = false;
            // 
            // pnlDiscard
            // 
            this.pnlDiscard.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlDiscard.Controls.Add(this.lblDiscard);
            this.pnlDiscard.Controls.Add(this.pbDiscard);
            this.pnlDiscard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlDiscard.Location = new System.Drawing.Point(197, 562);
            this.pnlDiscard.Name = "pnlDiscard";
            this.pnlDiscard.Size = new System.Drawing.Size(239, 58);
            this.pnlDiscard.TabIndex = 26;
            // 
            // lblDiscard
            // 
            this.lblDiscard.AutoSize = true;
            this.lblDiscard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDiscard.Location = new System.Drawing.Point(79, 21);
            this.lblDiscard.Name = "lblDiscard";
            this.lblDiscard.Size = new System.Drawing.Size(100, 30);
            this.lblDiscard.TabIndex = 1;
            this.lblDiscard.Text = "DISCARD";
            // 
            // pbDiscard
            // 
            this.pbDiscard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbDiscard.Image = ((System.Drawing.Image)(resources.GetObject("pbDiscard.Image")));
            this.pbDiscard.Location = new System.Drawing.Point(-9, 0);
            this.pbDiscard.Name = "pbDiscard";
            this.pbDiscard.Size = new System.Drawing.Size(82, 58);
            this.pbDiscard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDiscard.TabIndex = 0;
            this.pbDiscard.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(605, 38);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(37, 29);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 22;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(37, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(640, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 28);
            this.label2.TabIndex = 0;
            this.label2.Text = "NEW REMINDER";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(128, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(380, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "DIGITAL SECRETARY";
            // 
            // pnlRightOptions
            // 
            this.pnlRightOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRightOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pnlRightOptions.Controls.Add(this.pnlRightMain);
            this.pnlRightOptions.Location = new System.Drawing.Point(903, 12);
            this.pnlRightOptions.Name = "pnlRightOptions";
            this.pnlRightOptions.Size = new System.Drawing.Size(72, 721);
            this.pnlRightOptions.TabIndex = 19;
            // 
            // pnlRightMain
            // 
            this.pnlRightMain.Controls.Add(this.pbToggleSpeech);
            this.pnlRightMain.Controls.Add(this.pictureBox2);
            this.pnlRightMain.Controls.Add(this.pbExit);
            this.pnlRightMain.Controls.Add(this.pbHome);
            this.pnlRightMain.Controls.Add(this.pbLogout);
            this.pnlRightMain.Location = new System.Drawing.Point(3, 52);
            this.pnlRightMain.Name = "pnlRightMain";
            this.pnlRightMain.Size = new System.Drawing.Size(70, 591);
            this.pnlRightMain.TabIndex = 1;
            this.pnlRightMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlRightMain_Paint);
            // 
            // pbToggleSpeech
            // 
            this.pbToggleSpeech.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbToggleSpeech.Image = ((System.Drawing.Image)(resources.GetObject("pbToggleSpeech.Image")));
            this.pbToggleSpeech.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbToggleSpeech.Location = new System.Drawing.Point(0, 258);
            this.pbToggleSpeech.Name = "pbToggleSpeech";
            this.pbToggleSpeech.Size = new System.Drawing.Size(70, 76);
            this.pbToggleSpeech.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbToggleSpeech.TabIndex = 5;
            this.pbToggleSpeech.TabStop = false;
            this.pbToggleSpeech.MouseEnter += new System.EventHandler(this.pbToggleSpeech_MouseEnter);
            this.pbToggleSpeech.MouseLeave += new System.EventHandler(this.pbToggleSpeech_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 512);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pbExit
            // 
            this.pbExit.Image = ((System.Drawing.Image)(resources.GetObject("pbExit.Image")));
            this.pbExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbExit.Location = new System.Drawing.Point(-3, 388);
            this.pbExit.Name = "pbExit";
            this.pbExit.Size = new System.Drawing.Size(76, 76);
            this.pbExit.TabIndex = 5;
            this.pbExit.TabStop = false;
            // 
            // pbHome
            // 
            this.pbHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHome.Image = ((System.Drawing.Image)(resources.GetObject("pbHome.Image")));
            this.pbHome.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbHome.Location = new System.Drawing.Point(-3, 131);
            this.pbHome.Name = "pbHome";
            this.pbHome.Size = new System.Drawing.Size(76, 76);
            this.pbHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbHome.TabIndex = 4;
            this.pbHome.TabStop = false;
            this.pbHome.Click += new System.EventHandler(this.pbHome_Click);
            this.pbHome.MouseEnter += new System.EventHandler(this.pbHome_MouseEnter);
            this.pbHome.MouseLeave += new System.EventHandler(this.pbHome_MouseLeave);
            // 
            // pbLogout
            // 
            this.pbLogout.Image = ((System.Drawing.Image)(resources.GetObject("pbLogout.Image")));
            this.pbLogout.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pbLogout.Location = new System.Drawing.Point(0, 3);
            this.pbLogout.Name = "pbLogout";
            this.pbLogout.Size = new System.Drawing.Size(69, 76);
            this.pbLogout.TabIndex = 3;
            this.pbLogout.TabStop = false;
            // 
            // pbExitApplication
            // 
            this.pbExitApplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbExitApplication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbExitApplication.Image = ((System.Drawing.Image)(resources.GetObject("pbExitApplication.Image")));
            this.pbExitApplication.Location = new System.Drawing.Point(879, -31);
            this.pbExitApplication.Name = "pbExitApplication";
            this.pbExitApplication.Size = new System.Drawing.Size(43, 40);
            this.pbExitApplication.TabIndex = 2;
            this.pbExitApplication.TabStop = false;
            this.pbExitApplication.Click += new System.EventHandler(this.pbExitApplication_Click);
            this.pbExitApplication.MouseEnter += new System.EventHandler(this.pbExitApplication_MouseEnter);
            this.pbExitApplication.MouseLeave += new System.EventHandler(this.pbExitApplication_MouseLeave);
            this.pbExitApplication.MouseHover += new System.EventHandler(this.pbExitApplication_MouseHover);
            // 
            // pbMinimize
            // 
            this.pbMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbMinimize.Image = ((System.Drawing.Image)(resources.GetObject("pbMinimize.Image")));
            this.pbMinimize.Location = new System.Drawing.Point(830, -18);
            this.pbMinimize.Name = "pbMinimize";
            this.pbMinimize.Size = new System.Drawing.Size(43, 40);
            this.pbMinimize.TabIndex = 2;
            this.pbMinimize.TabStop = false;
            this.pbMinimize.Click += new System.EventHandler(this.pbMinimize_Click);
            this.pbMinimize.MouseEnter += new System.EventHandler(this.pbMinimize_MouseEnter);
            this.pbMinimize.MouseLeave += new System.EventHandler(this.pbMinimize_MouseLeave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 30);
            this.label3.TabIndex = 23;
            this.label3.Text = "Title";
            // 
            // rtxtNote
            // 
            // 
            // 
            // 
            this.rtxtNote.BackgroundStyle.Class = "RichTextBoxBorder";
            this.rtxtNote.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rtxtNote.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtNote.ForeColor = System.Drawing.Color.ForestGreen;
            this.rtxtNote.Location = new System.Drawing.Point(163, 421);
            this.rtxtNote.Name = "rtxtNote";
            this.rtxtNote.Rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Arial;}}\r\n{" +
    "\\colortbl ;\\red34\\green139\\blue34;}\r\n\\viewkind4\\uc1\\pard\\cf1\\fs32\\par\r\n}\r\n";
            this.rtxtNote.Size = new System.Drawing.Size(666, 100);
            this.rtxtNote.TabIndex = 25;
            // 
            // txtTitle
            // 
            // 
            // 
            // 
            this.txtTitle.Border.Class = "TextBoxBorder";
            this.txtTitle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTitle.Font = new System.Drawing.Font("Adobe Caslon Pro", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.ForestGreen;
            this.txtTitle.Location = new System.Drawing.Point(163, 133);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PreventEnterBeep = true;
            this.txtTitle.Size = new System.Drawing.Size(666, 30);
            this.txtTitle.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(85, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 30);
            this.label5.TabIndex = 23;
            this.label5.Text = "Date";
            // 
            // dtDateTime
            // 
            // 
            // 
            // 
            this.dtDateTime.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtDateTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtDateTime.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtDateTime.ButtonDropDown.Visible = true;
            this.dtDateTime.Format = DevComponents.Editors.eDateTimePickerFormat.Long;
            this.dtDateTime.IsPopupCalendarOpen = false;
            this.dtDateTime.Location = new System.Drawing.Point(163, 209);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtDateTime.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtDateTime.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtDateTime.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtDateTime.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtDateTime.MonthCalendar.DisplayMonth = new System.DateTime(2015, 8, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtDateTime.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtDateTime.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtDateTime.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtDateTime.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtDateTime.MonthCalendar.TodayButtonVisible = true;
            this.dtDateTime.Name = "dtDateTime";
            this.dtDateTime.Size = new System.Drawing.Size(666, 35);
            this.dtDateTime.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtDateTime.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 455);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 30);
            this.label6.TabIndex = 23;
            this.label6.Text = "Note";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 321);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 30);
            this.label4.TabIndex = 23;
            this.label4.Text = "Time";
            // 
            // tmTime
            // 
            // 
            // 
            // 
            this.tmTime.BackgroundStyle.Class = "DateTimeInputBackground";
            this.tmTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tmTime.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.tmTime.ButtonDropDown.Visible = true;
            this.tmTime.DateTimeSelectorVisibility = DevComponents.Editors.DateTimeAdv.eDateTimeSelectorVisibility.TimeSelector;
            this.tmTime.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime;
            this.tmTime.IsPopupCalendarOpen = false;
            this.tmTime.Location = new System.Drawing.Point(163, 321);
            // 
            // 
            // 
            // 
            // 
            // 
            this.tmTime.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tmTime.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.tmTime.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.tmTime.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tmTime.MonthCalendar.DisplayMonth = new System.DateTime(2015, 8, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.tmTime.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.tmTime.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.tmTime.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.tmTime.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tmTime.MonthCalendar.TodayButtonVisible = true;
            this.tmTime.MonthCalendar.Visible = false;
            this.tmTime.Name = "tmTime";
            this.tmTime.Size = new System.Drawing.Size(666, 35);
            this.tmTime.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.tmTime.TabIndex = 29;
            // 
            // frmNewReminder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 700);
            this.Controls.Add(this.pbMinimize);
            this.Controls.Add(this.pbExitApplication);
            this.Controls.Add(this.pnlRightOptions);
            this.Controls.Add(this.pnlMain);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "frmNewReminder";
            this.Text = "Digital Secretary";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlCreate.ResumeLayout(false);
            this.pnlCreate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCreate)).EndInit();
            this.pnlDiscard.ResumeLayout(false);
            this.pnlDiscard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDiscard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRightOptions.ResumeLayout(false);
            this.pnlRightMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbToggleSpeech)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExitApplication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlRightOptions;
        private System.Windows.Forms.Panel pnlRightMain;
        private System.Windows.Forms.PictureBox pbExit;
        private System.Windows.Forms.PictureBox pbHome;
        private System.Windows.Forms.PictureBox pbLogout;
        private System.Windows.Forms.PictureBox pbExitApplication;
        private System.Windows.Forms.PictureBox pbToggleSpeech;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbMinimize;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel pnlDiscard;
        private System.Windows.Forms.Label lblDiscard;
        private System.Windows.Forms.PictureBox pbDiscard;
        private System.Windows.Forms.Panel pnlCreate;
        private System.Windows.Forms.Label lblCreate;
        private System.Windows.Forms.PictureBox pbCreate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtDateTime;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx rtxtNote;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput tmTime;
    }
}

