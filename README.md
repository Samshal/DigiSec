# DigiSec
DigiSec, which is an alias of "Digital Secretary" is a desktop based dotnet application that's built to replace secretaries in virtually everything that's possible. Currently, it has a built in schedule management module, project management and time planner module, interactive reminders / notifications which uses voice input/output.
